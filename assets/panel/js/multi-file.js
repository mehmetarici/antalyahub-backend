function printFileNames() {
    var files = $("#uploadme")[0].files;
    [...files].forEach(function(item) {
        $('#files').append(
            '<div class="row">' +
            '<div class="col-md-6">'+
            '<b>'+item.name+'</b>' +
            '</div>'+
            '<div class="col-md-6">'+
            '<div class="input-group">' +
            '<span class="btn input-group-addon remove">x</span>'+
            '</div>' +
            '</div>'+
            '</div>'
        );
    });
}

//Click Functions

$('#clickme').click(function(){
    $('#uploadme').click();
});

$('body').on('click', '.remove', function () {
    $(this).parents().eq(2).remove();
});