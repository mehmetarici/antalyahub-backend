<!DOCTYPE html>
<html class="no-js">

<head>
    <?php $this->load->view("includes/head"); ?>
</head>

<body>
<!-- Header Area -->
<?php $this->load->view("includes/header"); ?>
<!-- Content Area -->
<?php $this->load->view("contact/breadcrumb"); ?>
<?php $this->load->view("contact/content"); ?>
<!-- Footer area-->
<?php $this->load->view("includes/footer"); ?>

</body>

</html>
