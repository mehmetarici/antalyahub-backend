<!DOCTYPE html>
<html class="no-js">

<head>
    <?php $this->load->view("includes/head"); ?>
</head>

<body>
<!-- Header Area -->
<?php $this->load->view("includes/header"); ?>
<!-- Content Area -->
<?php $this->load->view("service/breadcrumb"); ?>
<?php $this->load->view("service/content"); ?>
<!-- Footer area-->
<?php $this->load->view("includes/footer"); ?>

</body>

</html>
