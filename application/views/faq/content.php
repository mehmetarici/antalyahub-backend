<div class="content-area recent-property" style="background-color: #FCFCFC; padding: 55px 0;">
    <div class="container">
        <div class="row row-feat">
            <div class="col-md-12">
                <?php $i = 0;
                foreach ($contents as $content) { ?>
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title fqa-title" data-toggle="collapse" data-target="#<?php echo $content->id ?>">
                                <?php echo $content->title ?>
                            </h4>
                        </div>
                        <div id="<?php echo $content->id ?>" class="panel-collapse collapse fqa-body <?php if ($i == 0) echo "in"; ?>">
                            <div class="panel-body">
                                <?php echo $content->content ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
                } ?>
            </div>
        </div>
    </div>
</div>