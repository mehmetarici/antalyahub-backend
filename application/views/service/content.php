<div class="properties-area recent-property" style="background-color: #FFF;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 padding-top-40 properties-page">
                <div class="section clear">
                    <div id="list-type" class="proerty-th-list">
                        <div class="tab-content">
                            <div class="tab-pane active text-style" id="tab1">
                                <?php foreach ($contents as $content) { ?>
                                    <div class="col-sm-6 col-md-4 p0">
                                        <div class="box-two proerty-item">
                                            <div class="item-thumb">
                                                <a href="javascript:void()"><img src="<?php echo base_url("uploads/".$content->media)?>"></a>
                                            </div>
                                            <div class="item-entry overflow services">
                                                <h5> <?php echo $content->title ?> </h5>
                                                <div class="dot-hr"></div>
                                                <?php echo $content->content ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>