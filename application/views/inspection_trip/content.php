<div class="content-area single-property" style="background-color: #FCFCFC;">&nbsp;
    <div class="container">
        <div class="clearfix padding-top-40">
            <div class="col-md-8 single-property-content prp-style-1 ">
                <div class="single-property-wrapper">
                    <!-- .property-meta -->
                    <div class="section">
                        <div class="s-property-content">
                           <?php foreach ($contents as $content) {
                                echo $content->content;
                            } ?>
                        </div>
                    </div>
                    <!-- End description area  -->
                </div>
            </div>
            <?php $this->load->view('includes/search')?>
        </div>

    </div>
</div>