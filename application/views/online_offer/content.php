<div class="content-area recent-property padding-top-40" style="background-color: #FFF;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="" id="contact1">
                    <h2>Online Offer Form</h2>
                    <form method="post" action="<?php echo base_url("home/send_online_offer")?>">
                        <input type="text" class="hidden form-control" name="fromMail" value="<?php echo $setting->site_email?>">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="firstname">Name</label>
                                    <input type="text" class="form-control" name="name" id="firstname" placeholder="please type your name and surname..">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <select id="subject" name="subject" class="selectpicker show-tick form-control">
                                        <option value="Any">Any</option>
                                        <option value="Property Type">Property Type</option>
                                        <option value="City, Area">City, Area</option>
                                        <option value="Price Range">Price Range</option>
                                        <option value="Room Type">Room Type</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" class="form-control" id="email" placeholder="please type your mail">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="lastname">Phone</label>
                                    <input type="text" name="phone" class="form-control" id="lastname" placeholder="please type your phone number">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea id="message" name="message" rows="8" class="form-control" placeholder="Can you give me more detail about subject..."></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <button type="submit" name="sendOffer" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send Offer</button>
                            </div>
                        </div>
                        <!-- /.row -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>