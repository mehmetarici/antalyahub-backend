<!-- Footer area-->
<div class="footer-area">
    <div class=" footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>About us</h4>
                        <div class="footer-title-line"></div>
                        <img src="<?php echo base_url('uploads/'.$setting->logo) ?>" alt="" class="wow pulse" data-wow-delay="1s">
                        <p><?php echo $setting->description?></p>
                        <ul class="footer-adress">
                            <li><i class="pe-7s-mail strong"> </i> info@antalyahub.com</li>
                            <li><i class="pe-7s-call strong"> </i> +09 555 555 5555</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Quick links</h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-menu">
                            <li><a href="<?php echo base_url("home")?>">Home</a> </li>
                            <li><a href="sales">For Sale</a> </li>
                            <li><a href="inspection.html">Inspection Trip</a></li>
                            <li><a href="service.html">Services</a></li>
                            <li><a href="faq.html">fqa</a> </li>
                            <li><a href="contact.html">Contact</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 wow fadeInRight animated">
                    <div class="single-footer news-letter">
                        <h4>Stay in touch</h4>
                        <div class="footer-title-line"></div>
                        <p>Lorem ipsum dolor sit amet, nulla suscipit similique quisquam molestias. Vel unde, blanditiis.</p>
                        <form>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="E-mail ... ">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary subscribe" type="button"><i class="pe-7s-paper-plane pe-2x"></i></button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </form>

                        <div class="social pull-right">
                            <ul>
                                <li><a class="wow fadeInUp animated" href="<?php echo $setting->twitter ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="<?php echo $setting->facebook ?>" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="wow fadeInUp animated" href="<?php echo $setting->instagram ?>" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="footer-copy text-center">
    <div class="container">
        <div class="row">
            <div class="pull-left">
                <span><a href="#">Projital Bilişim</a> , Tüm hakları sakladır - <?php echo date("Y")?>  </span>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('includes/include_script') ?>