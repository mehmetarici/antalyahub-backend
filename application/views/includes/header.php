<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- Body content -->
<div class="header-connect">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8  col-xs-12">
                <div class="header-half header-call">
                    <p>
                        <span><i class="pe-7s-call"></i><?php echo $setting->site_phone ?></span>
                        <span><i class="pe-7s-mail"></i><?php echo $setting->site_email ?></span>
                    </p>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12">
                <div class="header-half header-social">
                    <ul class="list-inline">
                        <li><a href="<?php echo $setting->facebook ?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo $setting->twitter ?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo $setting->instagram ?>"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End top header -->

<nav class="navbar navbar-default ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url('home')?>"><img src="<?php echo base_url("uploads/".$setting->logo) ?>" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <ul class="main-nav nav navbar-nav navbar-right">
                <li class="wow fadeInDown" data-wow-delay="0.1s"><a <?php if($this->uri->segment(1)=="home" || $this->uri->segment(1)==""){echo 'class="active"';}?> href="<?php echo base_url("home")?>">Home</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.2s"><a <?php if($this->uri->segment(1)=="sales"){echo 'class="active"';} ?> href="sales.html">For Sale</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.3s"><a <?php if($this->uri->segment(1)=="inspection-trip"){echo 'class="active"';} ?> href="<?php echo base_url("inspection-trip")?>">Inspection Trip</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.4s"><a <?php if($this->uri->segment(1)=="our-services"){echo 'class="active"';} ?> href="<?php echo base_url("our-services")?>">Services</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.5s"><a <?php if($this->uri->segment(1)=="frequently-asked-questions"){echo 'class="active"';} ?> href="<?php echo base_url("frequently-asked-questions")?>">FAQ</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.6s"><a <?php if($this->uri->segment(1)=="our-contact"){echo 'class="active"';} ?> href="<?php echo base_url("our-contact")?>">Contact</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!-- End of nav bar -->