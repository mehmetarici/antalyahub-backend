<script src="<?php echo base_url('assets/site/js/modernizr-2.6.2.min.js')?>"></script>

<script src="<?php echo base_url('assets/site/js/jquery-1.10.2.min.js')?>"></script>
<script src="<?php echo base_url('assets/site/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/site/js/bootstrap-select.min.js')?>"></script>
<script src="<?php echo base_url('assets/site/js/bootstrap-hover-dropdown.js')?>"></script>

<script src="<?php echo base_url('assets/site/js/easypiechart.min.js')?>"></script>
<script src="<?php echo base_url('assets/site/js/jquery.easypiechart.min.js')?>"></script>

<script src="<?php echo base_url('assets/site/js/owl.carousel.min.js')?>"></script>
<script src="<?php echo base_url('assets/site/js/wow.js')?>"></script>

<script src="<?php echo base_url('assets/site/js/icheck.min.js')?>"></script>
<script src="<?php echo base_url('assets/site/js/price-range.js')?>"></script>

<script src="<?php echo base_url('assets/site/js/main.js')?>"></script>