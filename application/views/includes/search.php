<div class="col-md-4 p0">
    <aside class="sidebar sidebar-property blog-asside-right">
        <div class="panel panel-default sidebar-menu wow fadeInRight animated">
            <div class="panel-heading">
                <h3 class="panel-title">Smart search</h3>
            </div>
            <div class="panel-body search-widget">
                <form action="" class=" form-inline">
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control" placeholder="Key word">
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12" style="margin-bottom:10px;">
                                <select id="area" class="selectpicker" title="Property Type" data-live-search="true" data-live-search-style="begins">
                                    <option>Any</option>
                                    <option>Apartments</option>
                                    <option>Villas</option>
                                    <option>Land</option>
                                </select>
                            </div>
                            <div class="col-xs-12" style="margin-bottom:10px;">
                                <select id="bedrooms" title="City, Area" class="selectpicker show-tick form-control">
                                    <option>Any</option>
                                    <option>Antalya</option>
                                    <option>Side</option>
                                    <option>Kemer</option>
                                    <option>Alanya</option>
                                    <option>Manavgat</option>
                                    <option>Kaş</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>

                    <div class="row">
                        <div class="col-xs-12" style="margin-bottom:10px;">
                            <select id="bedrooms" class="selectpicker show-tick form-control" title="Room Type">
                                <option>Any</option>
                                <option>Studio</option>
                                <option>1+1</option>
                                <option>2+1</option>
                                <option>3+1</option>
                                <option>4+1</option>
                                <option>5+1</option>
                                <option>6+</option>
                            </select>
                        </div>
                        <div class="col-xs-12" style="margin-bottom:10px;">
                            <select id="price" class="selectpicker show-tick form-control" title="Price Range">
                                <option>Any</option>
                                <option>Up to 50.000 $</option>
                                <option>50.000-75.000 $</option>
                                <option>75.000-100.000 $</option>
                                <option>100.000-150.000 $</option>
                                <option>150.000-200.000 $</option>
                                <option>200.000-300.000 $</option>
                                <option>300.000-500.000 $</option>
                                <option>1.000.000 + $</option>
                            </select>
                        </div>
                    </div>
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-12">
                                <input class="button btn largesearch-btn" value="Search" type="submit">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

    </aside>
</div>
