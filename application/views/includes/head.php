<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $setting->site_header ?></title>
<meta name="description" content="<?php echo $setting->description ?>">
<meta name="author" content="<?php echo $setting->site_own ?>">
<meta name="keyword" content="<?php echo $setting->metas ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php $this->load->view('includes/include_style') ?>