<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="<?php echo base_url('assets/site/css/normalize.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/font-awesome.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/fontello.css') ?>">
<link href="<?php echo base_url('assets/site/fonts/icon-7-stroke/css/pe-icon-7-stroke.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/site/fonts/icon-7-stroke/css/helper.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/site/css/animate.css') ?>" rel="stylesheet" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/bootstrap-select.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/bootstrap/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/icheck.min_all.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/price-range.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/owl.carousel.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/owl.theme.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/owl.transitions.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/style.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/responsive.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/site/css/newStyle.css') ?>">