<div class="modal fade" id="product_add_modal" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header p-b-15">
                <h4 class="modal-title">İçerik Oluştur</h4>
                <ul class="card-actions icons right-top">
                    <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                        <i class="zmdi zmdi-close"></i>
                    </a>
                </ul>
            </div>
            <form method="post" action="<?php echo base_url('cpanel/icerik/olustur')?>" class="form-horizontal" enctype="multipart/form-data">
                <div class="modal-body p-0">
                    <div class="card card p-20 p-t-10 m-b-0">
                        <div class="card-body">
                            <input type="text" name="pageIn" class="hidden" value="inspection-trip" >
                            <input type="text" name="controller" class="hidden" value="inspection_trip" >
                            <div class="form-group label-floating is-empty">
                                <label class="control-label">Ana Başlık</label>
                                <input type="text" name="title" required class="form-control">
                            </div>
                            <div class="form-group is-empty">
                                <label class="control-label">Yayınlanma Tarihi</label>
                                <input type="date" name="publish_at" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                            </div>
                            <div class="form-group is-empty">
                                <label class="control-label">Açıklama</label>
                                <textarea id="summernote1" name="content"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kapat</button>
                        <button type="submit" name="add_product" class="btn btn-primary">İçerik Ekle</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- modal-dialog -->
    </div>
</div>