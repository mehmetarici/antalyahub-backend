<!DOCTYPE html>
<html lang="tr">

<head>
    <?php $this->load->view("panel/includes/head"); ?>
</head>
<body>
<div id="app_wrapper">
    <!-- HEADER -->
    <?php $this->load->view("panel/includes/header"); ?>
    <!-- END HEADER -->
    <section id="content_outer_wrapper">
        <div id="content_wrapper" class="card-overlay">
            <?php $this->load->view("panel/inspection_trip/breadcrumb") ?>
            <div id="content" class="container-fluid">
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card card-data-tables product-table-wrapper">
                                <?php $this->load->view("panel/inspection_trip/header") ?>
                                <!-- CONTENT -->
                                <?php $this->load->view("panel/inspection_trip/content"); ?>
                                <!-- END CONTENT -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view("panel/inspection_trip/add_modal") ?>
    <?php $this->load->view("panel/inspection_trip/update_modal") ?>
    <?php $this->load->view("panel/inspection_trip/delete_modal") ?>
    <!-- FOOTER -->
    <?php $this->load->view("panel/includes/footer"); ?>
    <!-- END FOOTER -->
</div>
</body>
</html>