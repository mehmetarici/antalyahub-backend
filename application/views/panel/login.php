<!DOCTYPE html>
<html lang="tr">
<head>
    <?php $this->load->view("panel/includes/head") ?>
</head>
<body id="auth_wrapper" >
<div id="login_wrapper">
    <div class="logo">
        <img src="<?php echo base_url("assets/panel/img/logo/logo-icon@2x.png")?>" alt="logo" class="logo-img">
    </div>
    <div id="login_content">
        <h1 class="login-title">
            AntalyaHub Admin Paneline Hoşgeldiniz...
        </h1>
        <div class="login-body">
            <?php if (isset($valid_errors))echo @'<div class="alert alert-danger" role="alert">'.@$valid_errors.'</div>'?>
            <form action="<?php echo base_url("cpanel/giris") ?>" method="post">
                <div class="form-group label-floating is-empty">
                    <label class="control-label">E-Posta</label>
                    <input type="email" name="email" class="form-control" data-rule-required="true" aria-required="true">
                </div>
                <div class="form-group label-floating is-empty">
                    <label class="control-label">Şifre</label>
                    <input type="password" name="password" class="form-control" data-rule-required="true"  aria-required="true">
                </div>
                <button type="submit" name="loginSubmit" class="btn btn-info btn-block m-t-40">Giriş Yap</button>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view("panel/includes/footer") ?>
</body>
</html>
