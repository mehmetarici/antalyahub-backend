<div class="tabpanel">
    <ul class="nav nav-tabs nav-justified">
        <li class="active" role="presentation"><a href="#tab-13" data-toggle="tab" aria-expanded="true">Yeni İlan Oluştur</a></li>
    </ul>
</div>
<form class="form-horizontal" action="<?php echo base_url('cpanel/satilik-ilanlar/olustur')?>" method="post" enctype="multipart/form-data">
    <div class="form-group is-empty">
        <label for="propertyName" class="col-md-2 control-label">İsim - Fiyat</label>
        <div class="col-md-5">
            <input type="text" class="form-control" id="propertyName" name="propertyName" placeholder="İlan İsmi"  required>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control" id="propertyPrice" name="propertyPrice" placeholder="İlan Fiyatı"  required>
        </div>
    </div>
    <div class="form-group is-empty form-material">
        <label for="siteName" class="col-md-2 control-label">İlan Çeşidi - İlan Alanı</label>
        <div class="col-md-5">
            <select class="form-control" id="select" name="propertyType">
                <option value="" disabled selected hidden>İlan çeşidini seçiniz</option>
                <option value="Apartments">Apartments</option>
                <option value="Villas">Villas</option>
                <option value="Land">Land</option>
            </select>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control" id="propertyArea" name="propertyArea" placeholder="İlan Alanı(m2)" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="propertyName" class="col-md-2 control-label">Oda Çeşidi - Oda Sayısı</label>
        <div class="col-md-5">
            <input type="text" class="form-control" id="propertyRoomType" name="propertyRoomType" placeholder="İlan Oda Çeşidi(örnek: 2+1)" required>
        </div>
        <div class="col-md-5">
            <input type="number" class="form-control" id="propertyPrice" name="propertyPrice" placeholder="İlan Oda Sayısı" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="propertyName" class="col-md-2 control-label">Banyo Sayısı - Garaj Sayısı</label>
        <div class="col-md-5">
            <input type="number" class="form-control" id="propertyBathroom" name="propertyBathroom" placeholder="İlan Banyo Sayısı" required>
        </div>
        <div class="col-md-5">
            <input type="number" class="form-control" id="propertyGarage" name="propertyGarage" placeholder="İlan Garaj Sayısı" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="textArea" class="col-md-2 control-label">İlan Açıklaması</label>
        <div class="col-md-10">
            <textarea class="form-control" rows="3" name="propertyDesc" id="textArea" placeholder="This home is really huge."></textarea>
        </div>
    </div>

    <div class="form-group is-empty">
        <label class="col-md-2 control-label">Genel Medya</label>
        <div class="col-md-10">
            <div class="form-group is-fileinput is-empty">
                <div class="input-group">
                    <input type="file" name="media" class="form-control dropify" data-height="100" />
                </div>
            </div>
        </div>
    </div>

    <div class="form-group is-empty">
        <label class="col-md-2 control-label">İlan Slider Medyaları</label>
        <div class="col-md-10">
            <div class="file-upload">
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <input type="file" id="uploadme" name="upl_files[]" onchange="printFileNames()" multiple/>
                        <input type="button" id="clickme" name="upl" value="Resimleri Seçiniz" class="btn btn-default"/>
                    </div>
                </div>
                <div id="files"></div>
            </div>
        </div>
    </div>

    <div class="form-group is-empty">
        <label for="propertyName" class="col-md-2 control-label">Bina Yaşı - Garaj Açıklaması</label>
        <div class="col-md-5">
            <input type="number" class="form-control" id="propertyBuiltYear" name="propertyBuiltYear" placeholder="İlan Bina Yaşı" required>
        </div>
        <div class="col-md-5">
            <input type="number" class="form-control" id="propertyGarageDesc" name="propertyGarageDesc" placeholder="İlan Garaj Açıklaması" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="propertyName" class="col-md-2 control-label">Manzara - WaterFront</label>
        <div class="col-md-5">
            <input type="text" class="form-control" id="propertyView" name="propertyView" placeholder="İlan Manzarası" required>
        </div>
        <div class="col-md-5">
            <div class="togglebutton">
                <label>
                    <input type="checkbox" class="toggle-info" checked>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="textArea" class="col-md-2 control-label">Extra Özellikler</label>
        <div class="col-md-10">
            <textarea class="form-control" rows="3" name="extraInfo" id="textArea" placeholder="Swimming pool, central cooling, ...."></textarea>
            <span class="help-block">Extra özellileri ayırmak için aralarına virgül koyunuz.</span>
        </div>
    </div>
    <div class="form-group is-empty">
        <label class="col-md-2 control-label">Yayınlanma Tarihi</label>
        <div class="col-md-10">
            <input type="date" name="publish_at" value="<?php echo date('Y-m-d'); ?>" class="form-control">
        </div>
    </div>

    <div class="form-group text-right">
        <div class="col-md-12">
            <button type="submit" name="addProperty" class="btn btn-primary">Yayınla</button>
        </div>
    </div>
</form>