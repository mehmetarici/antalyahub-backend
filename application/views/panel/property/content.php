<div class="card-body p-0">
    <div class="table-responsive">
        <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th></th>
                <th data-orderable="false" >İçerik Medyası</th>
                <th>İçerik Başlığı</th>
                <th>Yayın Tarihi</th>
                <th data-orderable="false" >Durumu</th>
                <th data-orderable="false" >
                    <button class="btn btn-primary btn-fab animate-fab place-add-modal-button" data-toggle="modal" data-target="#product_add_modal"><i class="zmdi zmdi-plus"></i></button>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            foreach ($contents as $content) { ?>
                <tr class="text-center">
                    <td class="checkbox-cell">
                        <?php echo $i;?>
                    </td>
                    <td><img src="<?php echo  base_url('uploads/'.$content->media)?>" alt="" class="img-thumbnail" /></td>
                    <td><?php echo $content->title?></td>
                    <td><?php echo date('d.m.Y', strtotime($content->publish_at))?></td>
                    <td>
                        <div class="togglebutton">
                            <label>
                                <input type="checkbox" onclick="change_status(<?php echo $content->id;?>)" class="toggle-info" <?php echo $content->status == 1 ? 'checked' : '' ?>>
                            </label>
                        </div>
                    </td>
                    <td>
                        <a id="modal" onclick="edit(<?php echo $content->id;?>)" class="icon"><i class="zmdi zmdi-edit"></i></a>
                        <a id="modal" data-toggle="modal" data-target="#product_delete_modal" data-id=<?php echo $content->id ;?> class="icon"><i class="zmdi zmdi-delete"></i></a>
                    </td>
                </tr>
                <?php
                $i++;
            } ?>
            </tbody>
        </table>
    </div>
</div>
