<header class="card-heading">
    <h2 class="card-title">İçeriği Yönet</h2>
    <small class="dataTables_info"></small>
    <div class="card-search">
        <div id="productsTable_wrapper" class="form-group label-floating is-empty">
            <i class="zmdi zmdi-search search-icon-left"></i>
            <input type="text" class="form-control filter-input" placeholder="İçerik Ara..." autocomplete="off">
            <a href="javascript:void(0)" class="close-search" data-card-search="close" data-toggle="tooltip" data-placement="top" title="Kapat"><i class="zmdi zmdi-close"></i></a>
        </div>
    </div>
    <ul class="card-actions icons right-top">
        <li id="deleteItems" style="display: none;">
            <span class="label label-info pull-left m-t-5 m-r-10 text-white"></span>
            <a href="javascript:void(0)" id="confirmDelete" data-toggle="tooltip" data-placement="top" data-original-title="İçerik(leri) Sil">
                <i class="zmdi zmdi-delete"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" data-card-search="open" data-toggle="tooltip" data-placement="top" data-original-title="İçerik Ara">
                <i class="zmdi zmdi-filter-list"></i>
            </a>
        </li>
        <li class="dropdown" data-toggle="tooltip" data-placement="top" data-original-title="Gösterilen İçerik Sayısı">
            <a href="javascript:void(0)" data-toggle="dropdown">
                <i class="zmdi zmdi-more-vert"></i>
            </a>
            <div id="dataTablesLength"></div>
        </li>
    </ul>
</header>
