<div class="tabpanel">
    <ul class="nav nav-tabs nav-justified">
        <li class="active" role="presentation"><a href="#tab-13" data-toggle="tab" aria-expanded="true">Site Ayarları</a></li>
    </ul>
</div>
<form class="form-horizontal" action="<?php echo base_url('cpanel/site-ayarlari/update')?>" method="post" enctype="multipart/form-data">
    <?php if (isset($valid_errors))echo @'<div class="alert alert-danger" role="alert">'.@$valid_errors.'</div>'?>
    <div class="form-group is-empty">
        <label for="siteName" class="col-md-2 control-label">Site İsmi</label>
        <div class="col-md-10">
            <input type="text" class="form-control" name="siteName" id="siteName" placeholder="Site İsmi" value="<?php echo $settings[0]->site_name?>" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="siteOwner" class="col-md-2 control-label">Site Sahibi</label>
        <div class="col-md-10">
            <input type="text" class="form-control" id="siteOwner" name="siteOwner" placeholder="Site Sahibi" value="<?php echo $settings[0]->site_own?>" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="emailAddress" class="col-md-2 control-label">E-Posta Adresi</label>
        <div class="col-md-10">
            <input type="email" class="form-control" id="emailAddress" name="emailAddress" placeholder="E-Posta Adresi" value="<?php echo $settings[0]->site_email?>" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label class="col-md-2 control-label">Şirket Logosu</label>
        <div class="col-md-10">
            <div class="form-group is-fileinput is-empty">
                <div class="input-group">
                    <input type="file" name="media" class="form-control dropify" data-height="100" data-default-file="<?php echo base_url("uploads/".$settings[0]->logo)?>" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="siteName" class="col-md-2 control-label">Sayfa Başlığı</label>
        <div class="col-md-10">
            <input type="text" class="form-control" id="siteHeader" name="siteTitle" placeholder="Sayfa Başlığı" value="<?php echo $settings[0]->site_header?>" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="siteName" class="col-md-2 control-label">Tel. No.</label>
        <div class="col-md-10">
            <input type="text" class="form-control" id="phoneNum" name="phoneNum" placeholder="Telefon No." value="<?php echo $settings[0]->site_phone?>" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="siteName" class="col-md-2 control-label">Sosyal Medya</label>
        <div class="col-md-3">
            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook Adresi" value="<?php echo $settings[0]->facebook?>" required>
        </div>
        <div class="col-md-3">
            <input type="text" class="form-control" id="instagram" name="instagram" placeholder="İnstagram Adresi" value="<?php echo $settings[0]->instagram?>" required>
        </div>
        <div class="col-md-3">
            <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter Adresi" value="<?php echo $settings[0]->twitter?>" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="textArea" class="col-md-2 control-label">Meta İsimler</label>
        <div class="col-md-10">
            <textarea class="form-control" rows="3" name="metaTags" id="textArea" placeholder="Antalya, Hub, Estate vb."><?php echo $settings[0]->metas?></textarea>
            <span class="help-block">Arama motorlarından arama yaparken, verilen metalar kullanılarak sonuçlar listelenir. (İsimler arasına virgül koyunuz.)</span>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="textArea" class="col-md-2 control-label">Meta Tanımı</label>
        <div class="col-md-10">
            <textarea class="form-control" rows="3" name="metaDesc" id="textArea" placeholder="AntalyaHub is really trusted estate firm in Turkey"><?php echo $settings[0]->description?></textarea>
            <span class="help-block">Meta tanımı siteniz kısa bir açıklamasıdır ve arama motorunda bu tanım çıkar.</span>
        </div>
    </div>
    <div class="form-group text-right">
        <div class="col-md-12">
            <button type="submit" name="updateSettings" class="btn btn-primary">Kaydet</button>
        </div>
    </div>
</form>