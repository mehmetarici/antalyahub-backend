<header id="app_topnavbar-wrapper">
    <nav role="navigation" class="navbar topnavbar">
        <div class="nav-wrapper">
            <ul class="nav navbar-nav pull-left left-menu">
                <li class="app_menu-open">
                    <a href="javascript:void(0)" data-toggle-state="app_sidebar-left-open" data-key="leftSideBar">
                        <i class="zmdi zmdi-menu"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown avatar-menu">
                    <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
                        <span class="meta">
                            <span class="avatar">
                                <img src="<?php echo base_url("assets/panel/img/profiles/02.jpg")?>" alt="" class="img-circle max-w-35">
                                <i class="badge mini success status"></i>
                            </span>
                            <span class="name">Mehmet Arıcı</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu btn-primary dropdown-menu-right">
                        <li>
                            <a href="app-mail.html"><i class="zmdi zmdi-email"></i> Gelen Kutusu</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><i class="zmdi zmdi-settings"></i> Site Ayarları</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("cpanel/logout")?>"><i class="zmdi zmdi-sign-in"></i> Çıkış Yap</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<aside id="app_sidebar-left">
    <div id="logo_wrapper">
        <ul>
            <li class="logo-icon">
                <a href="index.html">
                    <div class="logo"> <img src="<?php echo base_url('assets/panel/img/logo/logo-icon.png')?>" alt="Logo"> </div>
                    <h1 class="brand-text">AntalyaHub</h1>
                </a>
            </li>
            <li class="menu-icon">
                <a href="javascript:void(0)" role="button" data-toggle-state="app_sidebar-menu-collapsed" data-key="leftSideBar"> <i class="mdi mdi-backburger"></i> </a>
            </li>
        </ul>
    </div>
    <nav id="app_main-menu-wrapper" class="fadeInLeft scrollbar">
        <div class="sidebar-inner sidebar-push">
            <ul class="nav nav-pills nav-stacked">
                <li <?php if($this->uri->segment(2)=="site-ayarlari"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/site-ayarlari')?>"><i class="zmdi zmdi-settings"></i>Site Ayarları</a></li>
                <li <?php if($this->uri->segment(2)=="yeni_admin"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/yeni_admin')?>"><i class="zmdi zmdi-plus"></i>Admin Oluştur</a></li>
                <li class="sidebar-header">İÇERİK</li>
                <li <?php if($this->uri->segment(3)=="home"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/icerik/home')?>"><i class="zmdi zmdi-view-column"></i>Home</a></li>
                <li <?php if($this->uri->segment(3)=="inspection_trip"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/icerik/inspection_trip')?>"><i class="zmdi zmdi-widgets"></i>Inspection Trip</a></li>
                <li <?php if($this->uri->segment(3)=="service"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/icerik/service')?>"><i class="zmdi zmdi-view-quilt"></i>Services</a></li>
                <li <?php if($this->uri->segment(3)=="faq"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/icerik/faq')?>"><i class="zmdi zmdi-collection-image"></i>FAQ</a></li>
                <li class="sidebar-header">SATILIK İÇERİĞİ</li>
                <li <?php if($this->uri->segment(2)=="satilik-ilanlar"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/satilik-ilanlar')?>"><i class="zmdi zmdi-home"></i>Satılık İlanlarınız</a></li>
                <li <?php if($this->uri->segment(3)=="yeni_ilan"){echo 'class="active"';}?>><a href="<?php echo base_url('cpanel/satilik-ilanlar/yeni_ilan')?>"><i class="zmdi zmdi-plus"></i>İlan Ekle</a></li>

            </ul>
        </div>
    </nav>
</aside>