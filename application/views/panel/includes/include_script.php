<script src="<?php echo base_url("assets/panel/js/vendor.bundle.js")?>"></script>
<script src="<?php echo base_url("assets/panel/js/app.bundle.js")?>"></script>
<script src="<?php echo base_url("assets/panel/dropify/js/dropify.js")?>"></script>
<script src="<?php echo base_url("assets/panel/js/summernote.js")?>"></script>
<script src="<?php echo base_url("assets/panel/js/multi-file.js")?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#summernote1').summernote({
            height: 100,
            toolbar: [
                ['style', ['style']],
                ['fontsize', ['fontsize']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
        });
        $('#summernote2').summernote({
            height: 100,
            fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18','20','22', '24','30', '36', '48' , '64'],
            toolbar: [
                ['style', ['style']],
                ['fontsize', ['fontsize']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
        });
    });

</script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Sürükleyerek yada tıklayarak medyanızı yükleyin',
            'replace': 'Değiştimek için tıklayın yada sürükleyin',
            'remove':  'Kaldır',
            'error':   'Hatalı Dosya.'
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", "#modal", function () {
        var id = $(this).data('id');
        $(".sendID #id").val( id );
    });
    function edit(id)
    {
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo base_url('cpanel/icerik/guncelle/')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                if (data.content == null) {
                    var content = "";
                }else{
                    var content = data.content;
                }


                $('[name="title"]').val(data.title);
                $('[name="publish_at"]').val(data.publish_at);
                $('#summernote2').summernote('code', content);
                $('[name="id"]').val(data.id);

                $('#product_update_modal').modal('show'); // show bootstrap modal when complete loaded

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Girilen içeriğe ait bir id bulunmamaktadır.');
            }
        });
    }
    function change_status(id)
    {
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo base_url('cpanel/icerik/change_status/')?>/" + id,
            type: "GET",
            dataType: "JSON",

        });
    }
</script>