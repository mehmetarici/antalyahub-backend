<!DOCTYPE html>
<html lang="tr">

<head>
    <?php $this->load->view("panel/includes/head"); ?>
</head>
<body>
<div id="app_wrapper">
    <!-- HEADER -->
    <?php $this->load->view("panel/includes/header"); ?>
    <!-- END HEADER -->
    <section id="content_outer_wrapper">
        <div id="content_wrapper" class="card-overlay">
            <div id="content" class="container">
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body p-0">
                                    <!-- CONTENT -->
                                    <?php $this->load->view("panel/property_add/content"); ?>
                                    <!-- END CONTENT -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- FOOTER -->
    <?php $this->load->view("panel/includes/footer"); ?>
    <!-- END FOOTER -->
</div>
</body>
</html>