<div class="tabpanel">
    <ul class="nav nav-tabs nav-justified">
        <li class="active" role="presentation"><a href="#tab-13" data-toggle="tab" aria-expanded="true">Admin Oluştur</a></li>
    </ul>
</div>
<form class="form-horizontal" action="<?php echo base_url('cpanel/add_admin')?>" method="post">
    <?php if (isset($valid_errors))echo @'<div class="alert alert-danger" role="alert">'.@$valid_errors.'</div>'?>
    <div class="form-group is-empty">
        <label for="siteName" class="col-md-2 control-label">Admin İsmi</label>
        <div class="col-md-10">
            <input type="text" class="form-control" name="adminName" id="adminName" placeholder="Admin İsmi" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="siteOwner" class="col-md-2 control-label">Admin Email</label>
        <div class="col-md-10">
            <input type="text" class="form-control" id="email" name="email" placeholder="Admin Email" required>
        </div>
    </div>
    <div class="form-group is-empty">
        <label for="emailAddress" class="col-md-2 control-label">Şifre</label>
        <div class="col-md-10">
            <input type="password" class="form-control" id="password" name="password" placeholder="Admin Şifre" required>
        </div>
    </div>
    <div class="form-group text-right">
        <div class="col-md-12">
            <button type="submit" name="addAdmin" class="btn btn-primary">Kaydet</button>
        </div>
    </div>
</form>