<div class="modal fade" id="product_delete_modal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog modal-confirm" role="document">
        <form action="<?php echo base_url('cpanel/icerik/delete') ?>" method="post">
            <div class="modal-content">
                <div class="modal-body sendID">
                    <input type="text" name="controller" class="hidden" value="faq" >
                    <input id="id" class="hidden" name="id" type="text" value="">
                    <div class="swal2-icon swal2-warning pulse-warning" style="display: block;">!</div>
                    <h4 class="modal-title">İçeriği Silmek İstediğinize Emin misiniz?</h4>
                    <p>UYARI : Silinen içerik tekrar geri alınamaz.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-danger">Sil</button>
                </div>
            </div>
        </form>
    </div>
</div>