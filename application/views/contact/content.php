<div class="content-area recent-property padding-top-40" style="background-color: #FFF;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="" id="contact1">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3><i class="fa fa-phone"></i> Call center</h3>
                            <p class="text-muted">This number is toll free if calling from Turkey otherwise we advise you to send email</p>
                            <p><strong><?php echo $setting->site_phone?></strong></p>
                        </div>
                        <div class="col-sm-6">
                            <h3><i class="fa fa-envelope"></i> Electronic support</h3>
                            <p class="text-muted">Please feel free to write an email to us </p>
                            <ul>
                                <li><strong><a href="mailto:"><?php echo $setting->site_email?></a></strong> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.row -->

                    <h2>Contact form</h2>
                    <form method="post" action="<?php echo base_url("our-contact/touch_us")?>">
                        <div class="row">
                            <input type="text" class="form-control hidden" name="fromMail" value="<?php echo $setting->site_email?>">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="firstname">Name</label>
                                    <input type="text" class="form-control" name="firstName" id="firstname" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="lastname">Phone</label>
                                    <input type="text" class="form-control" name="phone" id="lastname" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" id="email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <input type="text" class="form-control" name="subject" id="subject" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea id="message" rows="8" name="message" class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <button type="submit" name="sendContact" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send message</button>
                            </div>
                        </div>
                        <!-- /.row -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>