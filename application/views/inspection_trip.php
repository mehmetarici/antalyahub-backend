<!DOCTYPE html>
<html class="no-js">

<head>
    <?php $this->load->view("includes/head"); ?>
</head>

<body>
<!-- Header Area -->
<?php $this->load->view("includes/header"); ?>
<!-- Content Area -->
<?php $this->load->view("inspection_trip/breadcrumb"); ?>
<?php $this->load->view("inspection_trip/content"); ?>
<!-- Footer area-->
<?php $this->load->view("includes/footer"); ?>

</body>

</html>
