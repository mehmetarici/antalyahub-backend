<div class="slider-area">
    <div class="slider">
        <div id="bg-slider" class="owl-carousel owl-theme">
            <?php foreach ($sliders as $slider) { ?>
                <div class="item"><img src="<?php echo base_url('uploads/'.$slider->media)?>" alt="GTA V"></div>
            <?php } ?>
        </div>
    </div>
    <div class="slider-content">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <h2>property Searching Just Got So Easy</h2>
                <div class="search-form wow pulse" data-wow-delay="0.8s">

                    <form action="" class=" form-inline">
                        <div class="form-group">
                            <label class="search-label" for="type">Property Type</label>
                            <select id="type" class="selectpicker show-tick form-control">
                                <option>Any</option>
                                <option>Apartments</option>
                                <option>Villas</option>
                                <option>Land</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="search-label" for="area">City, Area</label>
                            <select id="area" class="selectpicker" data-live-search="true" data-live-search-style="begins">
                                <option>Any</option>
                                <option>Antalya</option>
                                <option>Side</option>
                                <option>Kemer</option>
                                <option>Alanya</option>
                                <option>Manavgat</option>
                                <option>Kaş</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="search-label" for="bedrooms">Bedrooms</label>
                            <select id="bedrooms" class="selectpicker show-tick form-control">
                                <option>Any</option>
                                <option>Studio</option>
                                <option>1+1</option>
                                <option>2+1</option>
                                <option>3+1</option>
                                <option>4+1</option>
                                <option>5+1</option>
                                <option>6+</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="search-label" for="price">Price Range</label>
                            <select id="price" class="selectpicker show-tick form-control">
                                <option>Any</option>
                                <option>Up to 50.000 $</option>
                                <option>50.000-75.000 $</option>
                                <option>75.000-100.000 $</option>
                                <option>100.000-150.000 $</option>
                                <option>150.000-200.000 $</option>
                                <option>200.000-300.000 $</option>
                                <option>300.000-500.000 $</option>
                                <option>1.000.000 + $</option>
                            </select>
                        </div>
                        <button class="btn search-btn" type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
