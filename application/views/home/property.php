<div class="content-area home-area-1 recent-property" style="background-color: #FCFCFC; padding-bottom: 55px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 text-center page-title">
                <!-- /.feature title -->
                <h2>Top submitted property</h2>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium sed iure itaque consequatur quaerat voluptatibus ut similique.</p>
            </div>
        </div>

        <div class="row">
            <div class="proerty-th">
                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="detail.html"><img src="<?php echo base_url()?>assets/site/img/demo/property-1.jpg"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="detail.html">Super nice villa </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Area :</b> 120m </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="detail.html"><img src="<?php echo base_url()?>assets/site/img/demo/property-2.jpg"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="detail.html">Super nice villa </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Area :</b> 120m </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="detail.html"><img src="<?php echo base_url()?>assets/site/img/demo/property-3.jpg"></a>

                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="detail.html">Super nice villa </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Area :</b> 120m </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="detail.html"><img src="<?php echo base_url()?>assets/site/img/demo/property-4.jpg"></a>

                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="detail.html">Super nice villa </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Area :</b> 120m </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>


                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="detail.html"><img src="<?php echo base_url()?>assets/site/img/demo/property-2.jpg"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="detail.html">Super nice villa </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Area :</b> 120m </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="detail.html"><img src="<?php echo base_url()?>assets/site/img/demo/property-4.jpg"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="detail.html">Super nice villa </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Area :</b> 120m </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-two proerty-item">
                        <div class="item-thumb">
                            <a href="detail.html"><img src="<?php echo base_url()?>assets/site/img/demo/property-3.jpg"></a>
                        </div>
                        <div class="item-entry overflow">
                            <h5><a href="detail.html">Super nice villa </a></h5>
                            <div class="dot-hr"></div>
                            <span class="pull-left"><b>Area :</b> 120m </span>
                            <span class="proerty-price pull-right">$ 300,000</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 p0">
                    <div class="box-tree more-proerty text-center">
                        <div class="item-tree-icon">
                            <i class="fa fa-th"></i>
                        </div>
                        <div class="more-entry overflow">
                            <h5><a href="sales.html">CAN'T DECIDE ? </a></h5>
                            <h5 class="tree-sub-ttl">Show all properties</h5>
                            <a href="sales.html" class="btn border-btn more-black" value="All properties">All properties</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
