<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Anasayfa';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["home"] = "anasayfa/index";
$route["home/(.*)"] = "anasayfa/$1";
$route["inspection-t    rip"] = "inspection_trip/index";
$route["inspection-trip/(.*)"] = "inspection_trip/$1";
$route["our-services"] = "service/index";
$route["our-services/(.*)"] = "service/$1";
$route["frequently-asked-questions"] = "faq/index";
$route["frequently-asked-questions/(.*)"] = "faq/$1";
$route["our-contact"] = "contact/index";
$route["our-contact/(.*)"] = "contact/$1";

$route["cpanel/icerik"] = "panel/icerik/index";
$route["cpanel/icerik/(.*)"] = "panel/icerik/$1";

$route["cpanel/site-ayarlari"] = "panel/setting/index";
$route["cpanel/site-ayarlari/(.*)"] = "panel/setting/$1";

$route["cpanel/satilik-ilanlar"] = "panel/property/index";
$route["cpanel/satilik-ilanlar/(.*)"] = "panel/property/$1";

$route["cpanel"] = "panel/auth/index";
$route["cpanel/(.*)"] = "panel/auth/$1";

$route["^(.*)"] = "index/$1";