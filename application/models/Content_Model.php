<?php

class Content_Model extends CI_Model{

    public function get($where = array())
    {
        $result = $this->db
            ->where($where)
            ->get('contents')
            ->row();
        return $result;
    }

    public function get_all($where = array())
    {
        $result = $this->db
            ->where($where)
            ->get('contents')
            ->result();
        return $result;
    }

    public function get_todo_list($where1 = array(), $where2 = array())
    {
        $result = $this->db
            ->where($where1)
            ->or_where($where2)
            ->get('contents')
            ->result();
        return $result;
    }

    public function delete($where = array())
    {
        $result = $this->db
            ->where($where)
            ->delete('contents');
        return $result;
    }

    public function insert($data = array())
    {
        $result = $this->db
            ->insert('contents', $data);
        return $result;
    }

    public function update($where = array(), $data = array())
    {
        $result = $this->db
            ->where($where)
            ->update('contents', $data);
        return $result;
    }

    public function query($query)
    {
        $result = $this->db
            ->query($query)
            ->result();
        return $result;
    }

}