<?php
/**
 * Created by PhpStorm.
 * User: mehmet
 * Date: 01.12.2017
 * Time: 15:07
 */

class Auth_Model extends CI_Model{

    function loginControl($email, $password){
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get('admins');

        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function insert($data = array())
    {
        $result = $this->db
            ->insert('admins', $data);
        return $result;
    }
}