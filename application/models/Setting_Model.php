<?php

class Setting_Model extends CI_Model{
    public function get($where = array())
    {
        $result = $this->db
            ->where($where)
            ->get('settings')
            ->row();
        return $result;
    }

    public function get_all($where = array())
    {
        $result = $this->db
            ->where($where)
            ->get('settings')
            ->result();
        return $result;
    }

    public function delete($where = array())
    {
        $result = $this->db
            ->where($where)
            ->delete('settings');
        return $result;
    }

    public function insert($data = array())
    {
        $result = $this->db
            ->insert('settings', $data);
        return $result;
    }

    public function update($where = array(), $data = array())
    {
        $result = $this->db
            ->where($where)
            ->update('settings', $data);
        return $result;
    }

    public function query($query)
    {
        $result = $this->db
            ->query($query)
            ->result();
        return $result;
    }

}