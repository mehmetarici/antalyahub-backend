<?php
/**
 * Created by PhpStorm.
 * User: mehme
 * Date: 14.04.2018
 * Time: 18:04
 */

class Setting extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Setting_Model');
        if (empty($this->session->email))
        {
            redirect(base_url('cpanel'));
        }
    }
    public function index()
    {
        $where = array('id' => 1);
        $settings = $this->Setting_Model->get($where);

        $viewData['settings'] = array($settings);
        return $this->load->view("panel/settings", $viewData);
    }
    public function update()
    {
        if (isset($_POST['updateSettings']))
        {
            $config['upload_path'] = "uploads/";
            $config['allowed_types'] = "gif|jpg|png|jpeg";

            $this->load->library('upload', $config);

            $file = $this->upload->data();
            if(!$this->upload->do_upload('media'))
            {
                if ($file['file_name'] == '') {
                    $file['file_name'] = 'noimage.jpg';
                }
                else{
                    header("Refresh: 2; url=".base_url('cpanel/site-ayarlari').$this->input->post('controller'));
                    echo 'Yeni kayıt oluşturulurken bir hata oluştu. Tekrar Deneyiniz'.$this->upload->display_errors();
                    echo '<strong>Yönlendiriliyorsunuz...</strong>';
                    die();
                }
            }
            else
            {
                $file = $this->upload->data();
            }

            $data = array(
                'site_name'   => $this->input->post('siteName'),
                'site_own'    => $this->input->post('siteOwner'),
                'site_email'  => $this->input->post('emailAddress'),
                'site_header' => $this->input->post('siteTitle'),
                'metas'       => $this->input->post('metaTags'),
                'description' => $this->input->post('metaDesc'),
                'site_phone'  => $this->input->post('phoneNum')  ,
                'facebook'    => $this->input->post('facebook'),
                'twitter'     => $this->input->post('twitter'),
                'instagram'   => $this->input->post('instagram'),
                'logo'        => $file['file_name']
            );
            $where = array('id' => 1);
            $result = $this->Setting_Model->update($where, $data);
            if ($result == 1)
            {
                redirect(base_url('cpanel/site-ayarlari'));
            }
        }
    }
}