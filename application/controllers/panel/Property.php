<?php
/**
 * Created by PhpStorm.
 * User: mehme
 * Date: 14.04.2018
 * Time: 17:26
 */

class Property extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->email))
        {
            redirect(base_url('cpanel'));
        }
        $this->load->model('Property_Model');
    }

    public function index()
    {
        $contents = $this->Property_Model->get_all();

        $viewData["contents"] = $contents;

        $this->load->view('panel/property', $viewData);
    }

    public function yeni_ilan(){
        $this->load->view("panel/property_add");
    }
    private function upload_files(){
        $this->load->library('upload');
        $number_of_files_uploaded = count($_FILES['upl_files']['name']);
        // Faking upload calls to $_FILE
        if ($number_of_files_uploaded > 0){
            print_r($number_of_files_uploaded);
            die();
        }
        for ($i = 0; $i < $number_of_files_uploaded; $i++) :
          $_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
          $_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
          $_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
          $_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
          $_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];
          $config = array(
            'file_name'     => $_FILES['userfile']['name'] ,
            'allowed_types' => 'jpg|jpeg|png|gif',
            'max_size'      => 3000,
            'overwrite'     => FALSE,

            /* real path to upload folder ALWAYS */
            'upload_path' => 'uploads/'
          );
          $this->upload->initialize($config);
          if ( ! $this->upload->do_upload()) :
            print_r($this->upload->display_errors());
          else :
            $final_files_data[] = $this->upload->data();
            // Continue processing the uploaded data
              print_r($final_files_data);
          endif;
        endfor;
  }



    public function olustur()
    {
        if (isset($_POST['addProperty']))
        {
            /*
            $config['upload_path'] = "uploads/";
            $config['allowed_types'] = "gif|jpg|png|jpeg";

            $this->load->library('upload', $config);
            $this->upload->do_upload('extras');
            $file = $this->upload->data();
            print_r($file);*/
            $this->upload_files();
            /*
            if(!$this->upload->do_upload('media'))
            {
                if ($file['file_name'] == '') {
                    $file['file_name'] = 'noimage.jpg';
                }
                else{
                    header("Refresh: 2; url=".base_url('admin/icerik/'.$this->input->post('controller')));
                    echo 'Yeni kayıt oluşturulurken bir hata oluştu. Tekrar Deneyiniz'.$this->upload->display_errors();
                    echo '<strong>Yönlendiriliyorsunuz...</strong>';
                    die();
                }
            }
            else {
                $file = $this->upload->data();
            }
            $data = array(
                'title'         => $this->input->post('title'),
                'media'         => $file['file_name'],
                'status'        => 1,
                'pageIn'        => $this->input->post('pageIn'),
                'content'       => $this->input->post('content'),
                'publish_at'    => $this->input->post('publish_at'),
            );

            $result = $this->Content_Model->insert($data);

            if ($result == 1)
            {
                redirect(base_url('cpanel/icerik/'.$this->input->post('controller')));
            }*/
        }
    }

    public function guncelle($id)
    {
        $content = $this->Content_Model->get(["id" => $id]);
        echo json_encode($content);
    }

    public function icerik_guncelle()
    {
        $where = array("id" => $this->input->post("id"));

        if (isset($_POST['update_product']))
        {
            $this->form_validation->set_rules('title', 'İçerik Başlığı', 'required');
            $this->form_validation->set_rules('publish_at', 'Yayınlanma Tarihi', 'required');

            if ($this->form_validation->run() == true)
            {
                $config['upload_path'] = "uploads/";
                $config['allowed_types'] = "gif|jpg|png|jpeg";

                $content = $this->Content_Model->get(["id" => $this->input->post('id')]);
                if ($content->media != 'noimage.jpg') {
                    unlink("uploads/".$content->media);
                }
                $this->load->library('upload', $config);

                $file = $this->upload->data();
                if(!$this->upload->do_upload('media'))
                {
                    if ($file['file_name'] == '') {
                        $file['file_name'] = 'noimage.jpg';
                    }
                    else{
                        header("Refresh: 2; url=".base_url('cpanel/icerik/').$this->input->post('controller'));
                        echo 'Yeni kayıt oluşturulurken bir hata oluştu. Tekrar Deneyiniz'.$this->upload->display_errors();
                        echo '<strong>Yönlendiriliyorsunuz...</strong>';
                        die();
                    }
                }
                else
                {
                    $file = $this->upload->data();
                }
                $data = array(
                    'title'         => $this->input->post('title'),
                    'media'         => $file['file_name'],
                    'status'        => 1,
                    'pageIn'        => $this->input->post('pageIn'),
                    'content'       => $this->input->post('content'),
                    'publish_at'    => $this->input->post('publish_at'),
                );

                $result = $this->Content_Model->update($where, $data);

                if ($result == 1)
                {
                    redirect(base_url('cpanel/icerik/').$this->input->post('controller'));
                }
            }
        }
    }

    public function delete()
    {
        $content = $this->Content_Model->get(["id" => $this->input->post('id')]);
        if ($content->media != 'noimage.jpg') {
            unlink("uploads/".$content->media);
        }
        $this->Content_Model->delete(["id" => $this->input->post('id')]);
        redirect(base_url('cpanel/icerik/'.$this->input->post('controller')));
    }

    public function change_status($id)
    {
        $content = $this->Content_Model->get(["id" => $id]);
        $this->Content_Model->update(['id' => $id], ['status' => $content->status == 1 ? 0 : 1]);
        echo json_encode($content);
    }
}