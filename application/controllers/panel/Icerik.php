<?php
/**
 * Created by PhpStorm.
 * User: mehme
 * Date: 14.04.2018
 * Time: 17:26
 */

class Icerik extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->email))
        {
            redirect(base_url('cpanel'));
        }
        $this->load->model('Content_Model');
    }

    public function home()
    {
        $whereSlider = array(
            'pageIn'     => 'home-slider',
        );

        $contents = $this->Content_Model->get_all($whereSlider);

        $viewData["contents"] = $contents;

        $this->load->view('panel/index', $viewData);
    }

    public function inspection_trip(){
        $whereSlider = array(
            'pageIn'     => 'inspection-trip',
        );

        $contents = $this->Content_Model->get_all($whereSlider);

        $viewData["contents"] = $contents;
        $this->load->view("panel/inspection_trip", $viewData);
    }

    public function service(){
        $whereSlider = array(
            'pageIn'     => 'services',
        );

        $contents = $this->Content_Model->get_all($whereSlider);

        $viewData["contents"] = $contents;
        $this->load->view("panel/service", $viewData);
    }

    public function faq(){
        $whereSlider = array(
            'pageIn'     => 'faq',
        );

        $contents = $this->Content_Model->get_all($whereSlider);

        $viewData["contents"] = $contents;
        $this->load->view("panel/faq", $viewData);
    }

    public function olustur()
    {

        if (isset($_POST['add_product']))
        {

            $this->form_validation->set_rules('title', 'İçerik Başlığı', 'required');
            $this->form_validation->set_rules('publish_at', 'Yayınlanma Tarihi', 'required');

            if ($this->form_validation->run() == true)
            {

                $config['upload_path'] = "uploads/";
                $config['allowed_types'] = "gif|jpg|png|jpeg";

                $this->load->library('upload', $config);

                $file = $this->upload->data();
                if(!$this->upload->do_upload('media'))
                {
                    if ($file['file_name'] == '') {
                        $file['file_name'] = 'noimage.jpg';
                    }
                    else{
                        header("Refresh: 2; url=".base_url('admin/icerik/'.$this->input->post('controller')));
                        echo 'Yeni kayıt oluşturulurken bir hata oluştu. Tekrar Deneyiniz'.$this->upload->display_errors();
                        echo '<strong>Yönlendiriliyorsunuz...</strong>';
                        die();
                    }
                }
                else
                {
                    $file = $this->upload->data();
                }
                $data = array(
                    'title'         => $this->input->post('title'),
                    'media'         => $file['file_name'],
                    'status'        => 1,
                    'pageIn'        => $this->input->post('pageIn'),
                    'content'       => $this->input->post('content'),
                    'publish_at'    => $this->input->post('publish_at'),
                );

                $result = $this->Content_Model->insert($data);

                if ($result == 1)
                {
                    redirect(base_url('cpanel/icerik/'.$this->input->post('controller')));
                }
            }
        }
    }

    public function guncelle($id)
    {
        $content = $this->Content_Model->get(["id" => $id]);
        echo json_encode($content);
    }

    public function icerik_guncelle()
    {
        $where = array("id" => $this->input->post("id"));

        if (isset($_POST['update_product']))
        {
            $this->form_validation->set_rules('title', 'İçerik Başlığı', 'required');
            $this->form_validation->set_rules('publish_at', 'Yayınlanma Tarihi', 'required');

            if ($this->form_validation->run() == true)
            {
                $config['upload_path'] = "uploads/";
                $config['allowed_types'] = "gif|jpg|png|jpeg";

                $content = $this->Content_Model->get(["id" => $this->input->post('id')]);
                if ($content->media != 'noimage.jpg') {
                    unlink("uploads/".$content->media);
                }
                $this->load->library('upload', $config);

                $file = $this->upload->data();
                if(!$this->upload->do_upload('media'))
                {
                    if ($file['file_name'] == '') {
                        $file['file_name'] = 'noimage.jpg';
                    }
                    else{
                        header("Refresh: 2; url=".base_url('cpanel/icerik/').$this->input->post('controller'));
                        echo 'Yeni kayıt oluşturulurken bir hata oluştu. Tekrar Deneyiniz'.$this->upload->display_errors();
                        echo '<strong>Yönlendiriliyorsunuz...</strong>';
                        die();
                    }
                }
                else
                {
                    $file = $this->upload->data();
                }
                $data = array(
                    'title'         => $this->input->post('title'),
                    'media'         => $file['file_name'],
                    'status'        => 1,
                    'pageIn'        => $this->input->post('pageIn'),
                    'content'       => $this->input->post('content'),
                    'publish_at'    => $this->input->post('publish_at'),
                );

                $result = $this->Content_Model->update($where, $data);

                if ($result == 1)
                {
                    redirect(base_url('cpanel/icerik/').$this->input->post('controller'));
                }
            }
        }
    }

    public function delete()
    {
        $content = $this->Content_Model->get(["id" => $this->input->post('id')]);
        if ($content->media != 'noimage.jpg') {
            unlink("uploads/".$content->media);
        }
        $this->Content_Model->delete(["id" => $this->input->post('id')]);
        redirect(base_url('cpanel/icerik/'.$this->input->post('controller')));
    }

    public function change_status($id)
    {
        $content = $this->Content_Model->get(["id" => $id]);
        $this->Content_Model->update(['id' => $id], ['status' => $content->status == 1 ? 0 : 1]);
        echo json_encode($content);
    }
}