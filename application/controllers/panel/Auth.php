<?php
/**
 * Created by PhpStorm.
 * User: mehme
 * Date: 14.04.2018
 * Time: 19:58
 */
class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_Model');
    }

    public function index()
    {
        $this->load->view('panel/login');
    }

    public function giris()
    {
        if (isset($_POST["loginSubmit"]))
        {
            $this->form_validation->set_rules('email', 'E-Posta', 'required');
            $this->form_validation->set_rules('password', 'Şifre', 'required');

            if ($this->form_validation->run() == true)
            {
                $email = $this->input->post('email');
                $password = $this->input->post('password');

                $result = $this->Auth_Model->loginControl($email, $password);

                if ($result == 1)
                {
                    $sessionData = array('email' => $email);
                    $this->session->set_userdata($sessionData);
                    redirect(base_url('cpanel/site-ayarlari'));
                }else
                {
                    $viewData['valid_errors'] = 'Bu bilgilere sahip herhangi bir admin bulunmamaktadır.';
                    $this->load->view('panel/login', $viewData);
                }
            }
            else
            {
                $viewData['valid_errors'] = 'Tüm alanları eksiksiz şekilde doldurunuz.';
                $this->load->view('panel/login', $viewData);
            }
        }
    }

    public function logout(){
        session_destroy();
        redirect(base_url("cpanel"));
    }

    public function yeni_admin(){
        $this->load->view('panel/create_admin');
    }

    public function add_admin()
    {
        if (empty($this->session->email))
        {
            redirect(base_url('cpanel'));
        }
        if (isset($_POST["addAdmin"]))
        {
            $this->form_validation->set_rules('adminName', 'İsim', 'required');
            $this->form_validation->set_rules('email', 'E-Posta', 'required');
            $this->form_validation->set_rules('password', 'Şifre', 'required');

            if ($this->form_validation->run() == true)
            {
                $data = array(
                    "name"      => $this->input->post('adminName'),
                    "email"     => $this->input->post('email'),
                    "password"  => $this->input->post('password')
                );
                $result = $this->Auth_Model->insert($data);

                if ($result == 1)
                {
                    $viewData['valid_errors'] = 'Admin kaydetme işlemi başarılı bir şekilde gerçekleşmiştir.';
                    $this->load->view('panel/create_admin', $viewData);
                }else
                {
                    $viewData['valid_errors'] = 'Admin kaydetme işlemi başarılı bir şekilde gerçekleşemedi.';
                    $this->load->view('panel/create_admin', $viewData);
                }
            }
            else
            {
                $viewData['valid_errors'] = 'Tüm alanları eksiksiz şekilde doldurunuz.';
                $this->load->view('panel/create_admin', $viewData);
            }
        }
    }
}
