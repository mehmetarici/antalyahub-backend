<?php
/**
 * Created by PhpStorm.
 * User: mehme
 * Date: 14.04.2018
 * Time: 17:31
 */

class Inspection_trip extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Content_Model');
        $this->load->model('Setting_Model');
    }
    public function index()
    {
        $whereSlider = array(
            'pageIn'         => 'inspection-trip',
            'status'         => 1,
            'publish_at <= ' => date("Y-m-d")
        );
        $setting = $this->Setting_Model->get();
        $contents = $this->Content_Model->get_all($whereSlider);

        $viewData["contents"] = $contents;
        $viewData["setting"] = $setting;
        $this->load->view('inspection_trip', $viewData);
    }
}