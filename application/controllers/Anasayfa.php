<?php
/**
 * Created by PhpStorm.
 * User: mehme
 * Date: 14.04.2018
 * Time: 17:31
 */

class Anasayfa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Content_Model');
        $this->load->model('Setting_Model');
    }
    public function index()
    {
        $whereSlider = array(
            'pageIn'         => 'home-slider',
            'status'         => 1,
            'publish_at <= ' => date("Y-m-d")
        );
        $setting = $this->Setting_Model->get();
        $sliders = $this->Content_Model->get_all($whereSlider);

        $viewData["sliders"] = $sliders;
        $viewData["setting"] = $setting;
        $this->load->view('home', $viewData);
    }

    public function online_offer()
    {
        $setting = $this->Setting_Model->get();

        $viewData["setting"] = $setting;
        $this->load->view('online_offer', $viewData);
    }

    public function send_online_offer(){
        if (isset($_POST['sendOffer'])) {
            $config = array(
                "protocol" => "smtp",
                "smtp_host" => "ssl://smtp.googlemail.com",
                "smtp_port" => "465",
                "smtp_user" => "mehmetaricistd@gmail.com",
                "smtp_pass" => "Yasemin95.",
                "smarttls" => true,
                "charset" => "utf-8",
                "mailtype" => "html",
                "wordwrap" => true,
                "newline" => "\r\n",
            );

            $this->load->library('email', $config);
            $message = "<h3> İsim-Soyisim: ". "  ".$this->input->post('name')."</h3></br>".
                "<h4> Telefon - Email: "."  ".$this->input->post('phone')." - ".$this->input->post('email')."</h4></br>".
                "<h5> Konu: "."  ".$this->input->post('subject')."</h5></br>".
                "<h5>".$this->input->post('message')."</h5>";
            $this->email->from($this->input->post('email'), $this->input->post('name'));
            $this->email->to($this->input->post('fromMail'));
            $this->email->subject("AntalyaHub İstek Servisi".$this->input->post('subject'));
            $this->email->message($message);

            if ($this->email->send()) {
                redirect(base_url('home'));
            } else {
                echo $this->email->print_debugger();
            }
        }
    }
}